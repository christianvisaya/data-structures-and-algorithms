from movie_rating import solution


def test_solution_1():
    assert solution([500, -1000, 2000]) == [3000, 2000]


def test_solution_2():
    assert solution([500, 1000, -2000]) == [1000, 1500]


def test_solution_3():
    assert solution([-500, 1000, 2000]) == [4000, 2000]


def test_solution_4():
    assert solution([300, 300, 400, 400, 500, 500]) == [3900, 1500]


def test_solution_5():
    assert solution([-1300, -1000, 2000]) == [1200, 2000]
