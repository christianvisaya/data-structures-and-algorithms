def while_binary_search(numbers, target):
    left = 0
    right = len(numbers)
    while left <= right:
        mid = (left + right) // 2

        if numbers[mid] == target:
            return mid
        elif numbers[mid] > target:
            right = mid - 1
        else:
            left = mid + 1
    return -1


def recursion_binary_search(numbers, left, right, target):
    if left <= right:
        mid = (left + right) // 2
        if numbers[mid] == target:
            return mid
        elif numbers[mid] > target:
            return recursion_binary_search(numbers, left, mid - 1, target)
        else:
            return recursion_binary_search(numbers, mid + 1, right, target)
    else:
        return -1


numbers = [1, 2, 3, 4, 5]
target = 5

print(while_binary_search(numbers, target))
print(recursion_binary_search(numbers, 0, len(numbers) - 1, target))
