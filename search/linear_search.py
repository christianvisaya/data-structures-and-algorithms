from os import read


def for_linear_search(numbers, target):
    for i in range(len(numbers)):
        if numbers[i] == target:
            return i
    return -1


def for_enumerate_linear_search(numbers, target):
    for index, number in enumerate(numbers):
        if number == target:
            return index
    return -1


def while_linear_search(numbers, target):
    pointer = 0
    while pointer < len(numbers):
        if numbers[pointer] == target:
            return pointer
        else:
            pointer += 1
    return -1


numbers = [1, 2, 3, 4, 5]
target = 5

print(for_linear_search(numbers, target))
print(for_enumerate_linear_search(numbers, target))
print(while_linear_search(numbers, target))
